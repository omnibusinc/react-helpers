export function assignNestedValue(sourceObject, property, value, pushArray, resetValuesArray, delimiter = '.') {
    let sourceObjectRef = getNodeReference(sourceObject, property, delimiter);
    let finalProperty = _.last(_.split(property, delimiter));
    if(!!pushArray) {
        sourceObjectRef[finalProperty] = sourceObjectRef[finalProperty] || [];
        sourceObjectRef[finalProperty].push(value);
    } else {
        sourceObjectRef[finalProperty] = value;
    }
    if(!!resetValuesArray) {
        resetValuesArray.forEach(function(clearItem, idx) {
            let clearItemRef = getNodeReference(sourceObject, clearItem, delimiter);
            let finalProperty = _.last(_.split(clearItem, delimiter));
            clearItemRef[finalProperty] = null;
        });
    }
}

export function removeValueFromNestedArray(sourceObject, property, idx, delimiter = '.') {
    let sourceObjectRef = getNodeReference(sourceObject, property, delimiter);
    let finalProperty = _.last(_.split(property, delimiter));
    sourceObjectRef[finalProperty].splice(idx, 1);
}

export function removeItemFromNestedObject(sourceObject, path, delimiter = '.') {
    let sourceObjectRef = getNodeReference(sourceObject, path, delimiter);
    let finalProperty = _.last(_.split(path, delimiter));
    delete sourceObjectRef[finalProperty];
}

export function handleInputChange(Obj, item, callback, e) {
    let ObjReference = Object.assign({}, Obj);
    _.indexOf(item, '.') > -1
        ? assignNestedValue(ObjReference, item, e.currentTarget.value)
        : ObjReference[item] = e.currentTarget.value;
    this.callback(ObjReference, item);
}

export function getNumericSelectOptions(max) {
    let selectOptions = [];
    for(var i = 0; i < max; i++){
      selectOptions.push({ label: i, value: i });
    }
    return selectOptions;
}

export function convertFlatArrayToSelectOptions(flatArray) {
  let options = [];
  if(!flatArray) return options;
  for(var i = 0; i < flatArray.length; i++) {
    options.push({ label: flatArray[i], value: flatArray[i] });
  }
  return options;
}

export function convertMapToObject(sourceMap) {
  let newObj = {};
  for(var [key, value] of sourceMap) {
    newObj[key] = value;
  }
  return newObj;
}

export function convertObjectToMap(sourceObject) {
  let newMap = new Map();
  let mapKeys = _.keys(sourceObject);
  mapKeys.sort();
  _.each(mapKeys, (key) => {
    newMap.set(key, sourceObject[key]);
  });
  return newMap;
}

function getNodeReference(sourceObject, property, delimiter) {
  let pathArray = _.split(property, delimiter);
  let sourceObjectRef = sourceObject;
  _.each(_.take(pathArray, (pathArray.length - 1)), function(segment) {
    sourceObjectRef = sourceObjectRef[segment];
  });
  return sourceObjectRef;
}
